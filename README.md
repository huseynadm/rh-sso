# Ansible Role: RHSSO Installation

## Overview

This Ansible role simplifies the installation of Red Hat Single Sign-On (RHSSO) by orchestrating various tasks required for a seamless setup. The role assumes the presence of other roles (`rhsso-ansible`) that handle specific aspects of the installation, such as downloading files, copying configuration files, setting ownership, and creating a management user.

### Play:

- **RHSSO Install with Ansible Roles:**
  - Executes the role `rhsso-ansible` to automate the installation of RHSSO on the target hosts.
  - The `become: yes` directive ensures that the tasks are executed with elevated privileges.
  - `serial: 1` allows tasks to be executed on hosts one at a time.

## Usage

To use this role, include it in your Ansible playbook.

```yaml
- name: RHSSO Install with Ansible Roles
  hosts: your_target_hosts
  become: yes
  serial: 1
  roles:
    - rhsso-installation
```

## Notes

- Customize the `rhsso-ansible` role if specific configurations or additional tasks are needed for your RHSSO setup.
- Ensure that the necessary variables (e.g., RHSSO management user details) are defined in your playbook or inventory file.
- Verify that the roles included in `rhsso-installation` are correctly configured and meet your deployment requirements.

Feel free to extend this playbook to include other tasks or roles based on your specific RHSSO installation needs.

# 1. Ansible Role: Create Postgres Directory

## Overview

This Ansible role automates the creation of a directory required for configuring PostgreSQL in the Red Hat Single Sign-On (RHSSO) server.

### Task:

- **Create Postgres Directory:**
  - Uses the Ansible `file` module to ensure that the specified directory for PostgreSQL in the RHSSO server exists.
  - The path for the directory is set to "/data/rh-sso-7.5/modules/system/layers/keycloak/org/postgresql/main".

## Usage

To use this role, include it in your Ansible playbook.

```yaml
- name: Create Postgre Directory
  ansible.builtin.file:
    path: "/data/rh-sso-7.5/modules/system/layers/keycloak/org/postgresql/main" 
    state: directory

```

## Variables

No specific variables are required for this role. The path for the directory is hardcoded to "/data/rh-sso-7.5/modules/system/layers/keycloak/org/postgresql/main".

## Notes

- Ensure that the specified directory path aligns with your RHSSO server configuration.
- This role is a simple task and can be integrated into a larger playbook for setting up an entire RHSSO environment.

Feel free to customize the role to fit your specific requirements and directory structure.

# 2.Ansible Role: Create RHSSO User

## Overview

This Ansible role facilitates the creation of a system user named "rhsso" specifically tailored for the Red Hat Single Sign-On (RHSSO) service.

### Task:

- **Create RHSSO User:**
  - Utilizes the Ansible `user` module to create a system user with the following attributes:
    - Username: rhsso
    - Comment: "RH-SSO nologin service user"
    - Shell: /usr/sbin/nologin
    - Home Directory: Not created (`createhome: no`)

## Usage

To use this role, include it in your Ansible playbook.

```yaml
- name: Create rhsso user
  ansible.builtin.user:
    name: rhsso
    comment: "RH-SSO nologin service user"
    shell: /usr/sbin/nologin
    createhome: no

```

## Variables

No specific variables are required for this role. The user details are hardcoded as follows:
- Username: rhsso
- Comment: "RH-SSO nologin service user"
- Shell: /usr/sbin/nologin
- Home Directory: Not created (`createhome: no`)

## Notes

- The role is designed to create a dedicated system user for RHSSO services, enhancing security by assigning a non-login shell.
- Customize the role if additional user attributes or different configurations are needed for your environment.
- This role can be incorporated into a broader playbook for RHSSO server setup and configuration.

Feel free to modify the role to suit your specific requirements and security policies.
# 3.Ansible Role: Upgrade Packages and Install Software

## Overview

This Ansible role is designed to perform essential package management tasks, including upgrading all packages to their latest versions and installing specific software packages.

### Tasks:

1. **Upgrade All Packages:**
   - Utilizes the Ansible `yum` module to upgrade all installed packages to their latest versions.

2. **Install Unzip:**
   - Uses the Ansible `yum` module to install the "unzip" package.

3. **Install Java:**
   - Deploys the Ansible `yum` module to install the "java-11-openjdk" package.

## Usage

To use this role, include it in your Ansible playbook.

```yaml
- name: Upgrade All Packages
  ansible.builtin.yum:
    name: '*'
    state: latest
- name: Install Unzip
  ansible.builtin.yum:
    name: unzip
    state: present

- name: Install Java
  ansible.builtin.yum:
    name: java-11-openjdk
    state: present

```

## Variables

No specific variables are required for this role. The tasks are designed to operate with default settings for package management.

## Notes

- The role is intended for basic system setup, including upgrading all installed packages and installing common software packages like "unzip" and "Java."
- Customize the role to add or remove tasks based on your system requirements.
- Ensure that the package names are compatible with your Linux distribution.

Feel free to adapt the role to fit your specific needs, incorporating additional tasks or modifications as necessary for your environment.

# 4.Ansible Role: Download RH SSO Files

## Overview

This Ansible role automates the process of downloading Red Hat Single Sign-On (RHSSO) files and organizing the required directory structure for a seamless setup.

### Tasks:

1. **Download RH SSO Files Directory Structure:**
   - Uses the Ansible `file` module to create the necessary directory structure for RHSSO files.

2. **Download RHSSO Server Distribution:**
   - Utilizes the Ansible `get_url` module to download the RHSSO server distribution ZIP file (rh-sso-7.5.0-server-dist.zip) from the Red Hat Customer Portal.

3. **Download RHSSO Patch:**
   - Uses the Ansible `get_url` module to download the RHSSO patch ZIP file (rh-sso-7.5.3-patch.zip) from the Red Hat Customer Portal.

4. **Download PostgreSQL Driver:**
   - Deploys the Ansible `get_url` module to download the PostgreSQL JDBC driver (postgresql-42.6.0.jar) required for RHSSO.

## Usage

To use this role, include it in your Ansible playbook.

```yaml
---
- name: Download RH SSO Files
  file:
    path: "{{ item }}"
    state: directory
  loop:
    - /data
    - /data/rh-sso-7.5/modules/system/layers/keycloak/org/postgresql/main

- name: Download rh-sso-7.5.0-server-dist.zip
  get_url:
    url: https://access.redhat.com/cspdownload/5d4b27aa14bcf41cc38796ec033a1717/655bae8f/rhsso-7.5.0/rh-sso-7.5.0-server-dist.zip
    dest: "/data/rh-sso-7.5.0-server-dist.zip"
    mode: '0644'  # Optional, set the file permissions

- name: Download rh-sso-7.5.3-patch.zip
  get_url:
    url: https://access.redhat.com/cspdownload/82db5fa2f194bdbaedfd282d095f1fa6/655baf2f/rhsso-7.5.3/rh-sso-7.5.3-patch.zip
    dest: "/data/rh-sso-patch.zip"
    mode: '0644'  # Optional, set the file permissions

- name: Download postgresql-42.6.0.jar
  get_url:
    url: "https://jdbc.postgresql.org/download/postgresql-42.6.0.jar"
    dest: "/data/rh-sso-7.5/modules/system/layers/keycloak/org/postgresql/main/postgresql-42.6.0.jar"
    mode: '0644'  # Optional, set the file permissions

```

## Variables

No specific variables are required for this role. The file URLs and paths are hardcoded within the role.

## Notes

- The role assumes that the destination paths are suitable for your RHSSO setup. Modify the paths if necessary.
- Ensure that the download URLs are accurate and up-to-date based on your Red Hat Customer Portal access.

Feel free to customize the role to fit your specific RHSSO setup requirements.

# 5.Ansible Role: Unzip RHSSO Server Distribution

## Overview

This Ansible role focuses on unzipping the Red Hat Single Sign-On (RHSSO) server distribution ZIP file to facilitate the deployment process.

### Task:

- **Unzip RHSSO Server Distribution:**
  - Utilizes the Ansible `unarchive` module to extract the contents of the `rh-sso-7.5.0-server-dist.zip` file to the specified destination directory.

## Usage

To use this role, include it in your Ansible playbook.

```yaml
- name: Unzip file
  ansible.builtin.unarchive:
    src: /data/rh-sso-7.5.0-server-dist.zip
    dest: /data/
    remote_src: yes

```

## Variables

No specific variables are required for this role. The source (`src`) and destination (`dest`) paths are hardcoded within the role.

## Notes

- Ensure that the source path (`src`) points to the correct location of the RHSSO server distribution ZIP file.
- Adjust the destination path (`dest`) if you want to unzip the contents to a different directory.
- Verify that the file specified in `src` is present on the target host.

Feel free to integrate this role into a larger playbook for a comprehensive RHSSO deployment process. Customize the role as needed based on your specific deployment requirements.

# 6.Ansible Role: Copy RHSSO Configuration Files

## Overview

This Ansible role streamlines the process of copying Red Hat Single Sign-On (RHSSO) configuration files to their designated locations. It includes files for PostgreSQL, systemd service, and standalone-ha.xml.

### Tasks:

1. **Copy Module.xml for PostgreSQL:**
   - Utilizes the Ansible `copy` module to copy the `module.xml` file for PostgreSQL to the specified directory.

2. **Copy rhsso.service for systemd:**
   - Uses the Ansible `copy` module to copy the `rhsso.service` file for systemd to the `/etc/systemd/system` directory.
   - Triggers a notification to reload systemd after copying.

3. **Copy Standalone-ha.xml:**
   - Employs the Ansible `template` module to copy the `standalone-ha.xml` file to the standalone configuration directory.
   - Triggers a notification to restart the RHSSO service after copying.

## Usage

To use this role, include it in your Ansible playbook.

```yaml
- name: Copy Module.xml
  ansible.builtin.copy:
    src: "/tmp/rh-sso/role/rhsso-ansible/templates/module.xml"
    dest: "/data/rh-sso-7.5/modules/system/layers/keycloak/org/postgresql/main"

- name: Copy rhsso.service
  ansible.builtin.copy:
    src: "/tmp/rh-sso/role/rhsso-ansible/templates/rhsso.service"
    dest: "/etc/systemd/system"
  notify:
  - Reload-systemd

- name: Copy Standalone-ha.xml
  template: "src=/tmp/rh-sso/role/rhsso-ansible/templates/standalone-ha.xml dest=/data/rh-sso-7.5/standalone/configuration/standalone-ha.xml"
  notify: 
  - Restart-service

```

## Variables

No specific variables are required for this role. The source (`src`) and destination (`dest`) paths are hardcoded within the role.

## Notifications

- **Reload-systemd:**
  - Invoked after copying `rhsso.service` to reload the systemd configuration.

- **Restart-service:**
  - Triggered after copying `standalone-ha.xml` to restart the RHSSO service.

## Notes

- Customize the role if additional configuration files or modifications are necessary for your RHSSO setup.
- Ensure that the source paths (`src`) point to the correct locations of the configuration files.
- Verify that the destination paths (`dest`) are appropriate for your RHSSO configuration.

Feel free to integrate this role into a larger playbook for a comprehensive RHSSO setup. Adapt the role as needed to match your specific configuration requirements.

 # 6.1Ansible Role: Set Ownership for RHSSO Directories and Files

## Overview

This Ansible role simplifies the process of setting ownership for specific directories and files associated with Red Hat Single Sign-On (RHSSO). It ensures that the `rhsso` user and group have the appropriate ownership.

### Tasks:

1. **Set Ownership for /data Directory:**
   - Utilizes the Ansible `file` module to set ownership for the `/data` directory and its contents.
   - The owner and group are both set to `rhsso`.

2. **Set Ownership for rhsso.service File:**
   - Employs the Ansible `file` module to set ownership for the `rhsso.service` file in the `/etc/systemd/system` directory.
   - The owner and group are both set to `rhsso`.

## Usage

To use this role, include it in your Ansible playbook.

```yaml
- name: Set ownership for /data directory
  ansible.builtin.file:
    path: /data
    owner: rhsso
    group: rhsso
    recurse: yes
    state: directory

- name: Set ownership for rhsso.service file
  ansible.builtin.file:
    path: /etc/systemd/system/rhsso.service
    owner: rhsso
    group: rhsso
```

## Variables

No specific variables are required for this role. The paths and ownership details are hardcoded within the role.

## Notes

- Customize the role if additional directories or files need specific ownership settings.
- Ensure that the paths specified in the tasks align with your RHSSO setup.
- Verify that the `rhsso` user and group are present on the target host.

Feel free to integrate this role into a larger playbook for a comprehensive RHSSO setup. Adapt the role as needed to match your specific ownership requirements.

 # 6.2Ansible Role: Create RHSSO Management User

## Overview

This Ansible role automates the process of creating a management user for Red Hat Single Sign-On (RHSSO). It utilizes the `add-user.sh` script provided by RHSSO for this purpose.

### Task:

- **Create User for RHSSO Management:**
  - Uses the Ansible `command` module to execute the `add-user.sh` script with the specified management username (`rhsso_mgmt_user`) and password (`rhsso_mgmt_pass`).
  - `ignore_errors` is set to true to prevent the task from failing if the user already exists.

## Usage

To use this role, include it in your Ansible playbook.

```yaml
---
- name: Create User For RH SSO Management User
  command: sh /data/rh-sso-7.5/bin/add-user.sh -u {{ rhsso_mgmt_user }} -p {{ rhsso_mgmt_pass }}
  ignore_errors: true

```

## Variables

Ensure to define the following variables in your playbook or inventory file:

- `rhsso_mgmt_user`: The desired RHSSO management username.
- `rhsso_mgmt_pass`: The password for the RHSSO management user.

## Notes

- Customize the role if additional parameters or modifications are required for creating the management user.
- This role assumes that the `add-user.sh` script is present in the specified path (`/data/rh-sso-7.5/bin/`). Adjust the path if necessary.
- Verify that the RHSSO instance is running and accessible before executing this role.

Feel free to integrate this role into a larger playbook for a comprehensive RHSSO setup. Adapt the role as needed to match your specific management user requirements.


# 7.RHSSO Patching Ansible Role

## Overview

This Ansible role is designed to automate the process of patching Red Hat Single Sign-On (RHSSO) servers. It performs the following tasks:

1. **Get Current Patch Version:**
   - Retrieves the current patch version of RHSSO.

2. **Get Desired Patch Version:**
   - Retrieves the desired patch version specified in the `/vars/main.yaml` file. If you want to patch RHSSO, you can change the patch URL in this file.

3. **Patch RHSSO if Versions Differ:**
   - Checks if the current patch version differs from the desired patch version.
   - If there is a difference, it applies the patch using the `jboss-cli.sh` script.

4. **Check Server Status:**
   - Uses the `uri` module to perform a GET request to check if the RHSSO service is running successfully.

5. **Reboot the Node:**
   - Reboots the server to ensure that the applied patch takes effect.
   - Waits for the node to come back up.
   - Checks the status of the `rhsso.service`.
   - Uses the `uri` module to perform a GET request to check if the RHSSO service is running after the reboot.

## Usage

To use this role, include it in your Ansible playbook and set the desired patch version in the `/vars/main.yaml` file.

```yaml
- name: Get current patch version
  shell: "sh /data/rh-sso-7.5/bin/jboss-cli.sh 'patch info' | awk '{ print $2}' | head -n 1"
  register: current_patch_version

- name: Get desired patch version
  shell: sh /data/rh-sso-7.5/bin/jboss-cli.sh 'patch inspect /data/rh-sso-patch.zip' | awk '{ print $3}' | tail -n 2 | head -n 1
  register: desired_patch_version

- name: Patch RHSSO if versions differ
  shell: "sh /data/rh-sso-7.5/bin/jboss-cli.sh 'patch apply /data/rh-sso-patch.zip'"
  register: patching
  when: current_patch_version.stdout != desired_patch_version.stdout
  ignore_errors: yes

- debug:
    var: patching

- name: Use uri module to perform curl
  uri:
    url: "http://{{ ansible_host }}:8080"
    method: GET
    validate_certs: no  # Set to 'yes' if you want to validate SSL/TLS certificates
  register: result  # Store the result in a variable for later use

- name: Display the result
  debug:
    var: result

- name: Reboot the node
  block:
    - name: Reboot the node
      command: /sbin/reboot
      async: 0
      poll: 0
      ignore_errors: true
      timeout: 300   # Add a timeout of 300 seconds

    - name: Wait for the node to come back up
      wait_for_connection:
        delay: 30
        timeout: 300
    
    - name: Check rhsso.service status
      systemd:
        name: rhsso.service
        state: status
      register: rhsso_status

    - name: rhsso.service status
      debug:
        var: rhsso_status

    - name: Use uri module to perform curl
      uri:
        url: "http://{{ ansible_host }}:8080"
        method: GET
        status_code: 200  # You can specify the expected HTTP status code
      register: result
      retries: 3
      delay: 10
      until: result.status == 200

    - name: Display the result
      debug:
        var: result
```

## Variables

- `current_patch_version`: Stores the current RHSSO patch version.
- `desired_patch_version`: Stores the desired RHSSO patch version specified in `/vars/main.yaml`.
- `patching`: Stores the result of the patching process.
- `result`: Stores the result of the GET request to check the server status after patching.

## Notes

- Make sure to update the patch URL in the `/vars/main.yaml` file before running the playbook.
- The role includes a server reboot to ensure the applied patch takes effect. Adjust the timeout and delay values as needed for your environment.

Feel free to customize the role according to your specific requirements and environment.
